# Hortonworks Spark Certification test pattern, questions 3 and 4.#

*3rd and 4th tasks are on querying the hive tables, apply aggregate functionalities, conditions and ordering.*

Okay, let's train it!

**Input files:**    

(system users -> (id, role, department))

    [root@sandbox ~]# cat users.txt 
    1,tester,PD1
    2,manager,PD1
    3,tester2,PD1
    4,tester4,PD2
    5,tester5,PD2
    6,systemuser,PD1
    7,megauser,PD3
    8,seller,PD3
    9,product_manager,PD3
    10,system_manager,PD4

(login data of system users -> (system user id, loggin date, how long he stayed logged in))

    [root@sandbox ~]# cat logins.txt 
    1,2018-05-06,25
    2,2018-06-30,30
    3,2018-05-23,67
    4,2018-03-20,37
    5,2018-04-23,33
    6,2018-03-22,55
    7,2018-02-11,33
    8,2018-02-10,456
    9,2018-02-10,27
    [root@sandbox ~]# 

Now let's prepare these files for using inside of Apache Hive by copying them into HDFS:

    [root@sandbox ~]# hdfs dfs -mkdir /tests/users
    [root@sandbox ~]# hdfs dfs -copyFromLocal users.txt /tests/users/users.txt
    [root@sandbox ~]# hdfs dfs -mkdir /tests/logins
    [root@sandbox ~]# hdfs dfs -copyFromLocal logins.txt /tests/logins/logins.txt

## Task 1. Prepare Hive tables based on HDFS data

To make it more interesting, let's put both Hive tables into new Hive database called "people".

    import org.apache.spark.sql.hive.HiveContext;

    val hc = new HiveContext(sc);
    hc.sql("CREATE DATABASE IF NOT EXISTS people");

    hc.sql("CREATE EXTERNAL TABLE IF NOT EXISTS people.users(id INT, role String, department String) "+
       " ROW FORMAT DELIMITED "+
       " FIELDS TERMINATED BY ','"+
       " LOCATION '/tests/users'"
    );

    hc.sql("CREATE EXTERNAL TABLE IF NOT EXISTS people.login(id INT, date Date, duration INT) "+
        " ROW FORMAT DELIMITED "+
        " FIELDS TERMINATED BY ','"+
        " LOCATION '/tests/logins'"
    );

    hc.sql("USE people");
    hc.sql("SHOW TABLES").show();

Zeppelin output:

    import org.apache.spark.sql.hive.HiveContext
    hc: org.apache.spark.sql.hive.HiveContext = org.apache.spark.sql.hive.HiveContext@662d9ee1
    res59: org.apache.spark.sql.DataFrame = [result: string]
    res60: org.apache.spark.sql.DataFrame = [result: string]
    res61: org.apache.spark.sql.DataFrame = [result: string]
    res62: org.apache.spark.sql.DataFrame = [result: string]
    +---------+-----------+
    |tableName|isTemporary|
    +---------+-----------+
    |    login|      false|
    |    users|      false|
    +---------+-----------+

## Task 2. Hive table querying, left join task.

    // Get me all people from all departments who haven't logged into the system.

    import org.apache.spark.sql.hive.HiveContext;

    val hc = new HiveContext(sc);

    hc.sql("USE people");

    var notloggedDF = hc.sql("SELECT p.id, p.role, p.department FROM users p LEFT JOIN login l ON p.id = l.id WHERE l.id is null");

    notloggedDF.show();

Zeppelin output:

    import org.apache.spark.sql.hive.HiveContext
    hc: org.apache.spark.sql.hive.HiveContext = org.apache.spark.sql.hive.HiveContext@435f125d
    res57: org.apache.spark.sql.DataFrame = [result: string]
    notloggedDF: org.apache.spark.sql.DataFrame = [id: int, role: string, department: string]
    +---+--------------+----------+
    | id|          role|department|
    +---+--------------+----------+
    | 10|system_manager|       PD4|
    +---+--------------+----------+


## Task 3. Hive table querying, filtering task.

    // Get me all people from 'PD1' department who logged in.

    import org.apache.spark.sql.hive.HiveContext;

    val hc = new HiveContext(sc);

    val valPD1PeopleDF = hc.sql("SELECT p.role, l.duration, l.date FROM people.users p INNER JOIN people.login l ON p.id = l.id WHERE p.department = 'PD1'");

    valPD1PeopleDF.show();

Zeppelin output:

    import org.apache.spark.sql.hive.HiveContext
    hc: org.apache.spark.sql.hive.HiveContext = org.apache.spark.sql.hive.HiveContext@30456ec2
    valPD1PeopleDF: org.apache.spark.sql.DataFrame = [role: string, duration: int, date: date]
    +----------+--------+----------+
    |      role|duration|      date|
    +----------+--------+----------+
    |    tester|      25|2018-05-06|
    |   manager|      30|2018-06-30|
    |   tester2|      67|2018-05-23|
    |systemuser|      55|2018-03-22|
    +----------+--------+----------+

## Task 4. Hive table querying, ordering task with limiting the output.

    // Get me top three people from 'PD1' department with longest login duration..result (role, duration, date)

    import org.apache.spark.sql.hive.HiveContext;

    val hc = new HiveContext(sc);

    val dp1topThreeDF = hc.sql(" SELECT p.role, l.duration, l.date FROM people.users p INNER JOIN people.login l "+
                           " ON p.id = l.id WHERE p.department = 'PD1' ORDER BY l.duration DESC LIMIT 3");

    dp1topThreeDF.show();
    
Zeppelin output:

    import org.apache.spark.sql.hive.HiveContext
    hc: org.apache.spark.sql.hive.HiveContext = org.apache.spark.sql.hive.HiveContext@3b9bedc0
    dp1topThreeDF: org.apache.spark.sql.DataFrame = [role: string, duration: int, date: date]
    +----------+--------+----------+
    |      role|duration|      date|
    +----------+--------+----------+
    |   tester2|      67|2018-05-23|
    |systemuser|      55|2018-03-22|
    |   manager|      30|2018-06-30|
    +----------+--------+----------+

## Task 5. Hive table querying, grouping task.

    // Get me top login durations of people per department...result (department, duration)

    import org.apache.spark.sql.hive.HiveContext;

    val hc = new HiveContext(sc);

    hc.sql("USE people");
    val longestDurationDF = hc.sql(" select p.department, max(l.duration) as max_duration FROM users p "+
                               " INNER JOIN login l ON p.id = l.id GROUP BY p.department ");

    longestDurationDF.show();

Zeppelin output:

    import org.apache.spark.sql.hive.HiveContext
    hc: org.apache.spark.sql.hive.HiveContext = org.apache.spark.sql.hive.HiveContext@4b85d44d
    res90: org.apache.spark.sql.DataFrame = [result: string]
    longestDurationDF: org.apache.spark.sql.DataFrame = [department: string, max_duration: int]
    +----------+------------+
    |department|max_duration|
    +----------+------------+
    |       PD1|          67|
    |       PD2|          37|
    |       PD3|         456|
    +----------+------------+

## Task 6. Hive table querying, grouping task.

    // Get me top login durations of people per department...result (department, role, duration)

    import org.apache.spark.sql.hive.HiveContext;

    import org.apache.spark.sql.expressions.Window;
    import org.apache.spark.sql.functions._;

    val hc = new HiveContext(sc);

    hc.sql("USE people");

    val sourceDF = hc.sql("select p.department, p.role, l.duration FROM users p "+
                               " INNER JOIN login l ON p.id = l.id");

    val grpWindow = Window.partitionBy("department").orderBy(col("duration").desc);
    val indexedDF = sourceDF.withColumn("r", row_number().over(grpWindow)).filter("r = 1");

    indexedDF.registerTempTable("groupedResult");

    hc.sql("select department, role, duration from groupedResult").show();

Zeppelin output:

    import org.apache.spark.sql.hive.HiveContext
    import org.apache.spark.sql.expressions.Window
    import org.apache.spark.sql.functions._
    hc: org.apache.spark.sql.hive.HiveContext = org.apache.spark.sql.hive.HiveContext@69c26ce8
    res109: org.apache.spark.sql.DataFrame = [result: string]
    sourceDF: org.apache.spark.sql.DataFrame = [department: string, role: string, duration: int]
    grpWindow: org.apache.spark.sql.expressions.WindowSpec = org.apache.spark.sql.expressions.WindowSpec@8b1e825
    indexedDF: org.apache.spark.sql.DataFrame = [department: string, role: string, duration: int, r: int]
    +----------+-------+--------+
    |department|   role|duration|
    +----------+-------+--------+
    |       PD1|tester2|      67|
    |       PD2|tester4|      37|
    |       PD3| seller|     456|
    +----------+-------+--------+

Best regards

Tomas
                               
